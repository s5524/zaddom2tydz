﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.Models;
using Kurs.DataLayer.Repositories;

namespace Kurs.BusinessLayer.Services
{
    public class CourseService
    {
        public void AddCourse(string tempCourseName, string tempCourseOwner,DateTime tempDateStart,int tempPercenHomework,int tempPrecentAbsence)
        {
            var course = new Course();
            course.CourseName = tempCourseName;
            course.CourseOwner = tempCourseOwner;
            course.CourseDate = tempDateStart;
            course.PercentHomework = tempPercenHomework;
            course.PercentAbsence = tempPrecentAbsence;
            CourseReposietories dbusues = new CourseReposietories();
            dbusues.AddCourse(course);
        }

        public bool CheckIfCourseNameIsInDB(string name)
        {
            var courseRepository = new CourseReposietories(); //biore repo z niżej warstwy
            var courseName = courseRepository.checkCourseByName(name);
            return courseName;
        }

        
        public void AddHomeworks(string name,int score, int maxScore, DateTime day,int studentId)//Presence presence, int courseId, int studentId
        {
            var homework = new Homework();
            homework.MaxScore = maxScore;
            homework.StudentScore = score;
            homework.HomeworkDate = day;
            var coursesReposietories = new CourseReposietories();
            coursesReposietories.AddHomework(homework,name,studentId);
        }
        public void AddPresenDay(int id, int presence, DateTime date, string name)
        {
            Presence presences = new Presence();
            presences.PresencDate = date;
            presences.IsPresence = presence;
            var coursesReposietories = new CourseReposietories();
            coursesReposietories.AddPresenceDay(presences, name, id);

        }



    }
}
