﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer;
using Kurs.DataLayer.Models;
using Kurs.DataLayer.Repositories;

namespace Kurs.BusinessLayer.Services
{
    public class StudentService
    {
        public bool CheckIfStudentExistsInDBByPesel(int pesel)
        {
            var studentRepository = new StudentReposietories(); //biore repo z niżej warstwy
            var studentPesel = studentRepository.checkStudentsByPesel(pesel);
            return studentPesel;
        }

        public bool CheckHowMuchStudentsIs(int check)
        {
            var studentRepository = new StudentReposietories();
            var studentCount = studentRepository.checkStudentsCount(check);
            return studentCount;
        }

        public void addStudent(string tempName, string tempSurname, DateTime tempDataTime, int tempPesel, int tempSex)
        {
            Student student = new Student();
            student.Name = tempName;
            student.Surname = tempSurname;
            student.DateOfBirth = tempDataTime;
            student.Pesel = tempPesel;
            student.Sex = tempSex;
            StudentReposietories dbusues = new StudentReposietories();
            dbusues.AddStudents(student);
        }

        public void AddStudentByPeselToCourses(int pesel,string courseName)
        {
            var studentListByPesel = new StudentReposietories();
            studentListByPesel.AddStudentByPeselToCourse(pesel,courseName);
        }

        public List<int> GetStudentsInCourse(string courseName)
        {
            var costam = new StudentReposietories();
            List<int> list = costam.GetAllStudentsInCourse(courseName);
            return list;
        }


        public string GetStudentNameById(int id)
        {
            var studentName = new StudentReposietories();
            var name = studentName.GetName(id);
            return name;
        }
        public void AddPresenDay(int id, int presence, DateTime date,string name)
        {
            Presence presences = new Presence();
            presences.PresencDate = date;
            presences.IsPresence = presence;
            var coursesReposietories = new CourseReposietories();
            coursesReposietories.AddPresenceDay(presences,name,id);

        }

        public int HomeworkScore(int i, string courseName)
        {
            StudentReposietories percentHomework = new StudentReposietories();
            float points = percentHomework.HomeworkPoints(i, courseName);
            float maxPoints = percentHomework.HomeworkMaxPoints(i, courseName);
            float score = (points/maxPoints)*100;
            return (int)score;

        }

        //public int PresenceScore(int i, string courseName)
        //{

        //}
        public int PresenceScore(int i, string courseName)
        {
            StudentReposietories percentPresence = new StudentReposietories();
            float points = percentPresence.PresencePoints(i, courseName);
            float maxPoints = percentPresence.PresenceMaxPoints(i, courseName);
            float score = (points / maxPoints) * 100;
            
            return (int)score;
        }

        public string IsPased(int i, string courseName)
        {
            StudentReposietories percentPresence = new StudentReposietories();
            float points = percentPresence.PresencePoints(i, courseName);
            float maxPoints = percentPresence.PresenceMaxPoints(i, courseName);
            float score = (points / maxPoints) * 100;
            float pointsH = percentPresence.HomeworkPoints(i, courseName);
            float maxPointsH = percentPresence.HomeworkMaxPoints(i, courseName);
            float scoreH = (pointsH / maxPointsH) * 100;
            CourseReposietories ispased = new CourseReposietories();
            var percentPresences = ispased.GetMaxPresenceScore(courseName);
            var peercentHomework = ispased.GetMaxHomeworkScore(courseName);
            string pass;
            if(score>=percentPresences && scoreH >= peercentHomework)
            {
                pass = "ZALICZONE";
            }
            else
            {
                pass = "NIEZALICZONE";
            }
            return pass;
        }
    }

}
