﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs.DataLayer.Models
{
    public class Course
    {
        public int Id { get; set; }
        public String CourseName { get; set; }
        public String CourseOwner { get; set; }
        public DateTime CourseDate { get; set; }
        public int PercentHomework { get; set; }
        public int PercentAbsence { get; set; }
        public List<Student> Students { get; set; }
        public List<Presence> Presence { get; set; }
        public List<Homework> Homeworks { get; set; }

    }
}
