﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs.DataLayer.Models
{
    public class Homework
    {
        public int Id { get; set; }
        public DateTime HomeworkDate { get; set; }
        public int StudentScore { get; set; }
        public int MaxScore { get; set; }
        public Course Course { get; set; }
        public Student Student { get; set; }
    }
}
