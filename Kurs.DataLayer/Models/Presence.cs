﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs.DataLayer.Models
{
    public class Presence
    {
        public int id { get; set; }
        public DateTime PresencDate { get; set; }
        public int IsPresence { get; set; }
        public Course Course { get; set; }
        public Student Student { get; set; }
        //wszystkie dto mam w servisach


        //w taki sposób wydaje mi sie
        //prawie - czym jest obiekt presence? co reprezentuje?
        //reprezentuje tabele z data czy jest obecny dany student w danym kursie

        //ok - to dla czego lista? :) jeszcze duze litery i ok
        //teraz musisz przypisac obiekt kursu i studenta i bedzie ok
    }
}
