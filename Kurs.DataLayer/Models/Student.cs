﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kurs.DataLayer.Models
{
    public class Student
    {
        [Key]
        public int Id { get; set; }
        public int Pesel { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public DateTime DateOfBirth { get; set; }
        public int Sex { get; set; }
        public List<Course> Courses { get; set; }
        public List<Presence> Presences { get; set; }
        public List<Homework> Homeworks { get; set; }
       //potrzebna - bo student moze miec wiele "obecnosci", ale obecnosc zawsze dotyczy jednego studenta. Ta lista t relacja od studenta do obecnosci.
       //teoretycznie mam dodawanie obecności ok to testuje i jak bede miał jeszcze jakieś pytania to będe sie odzywał
       //czy potrzebuje coś jeszcze??:P wszystko co jest na opisie z PD :P ale dodawanie obecnosci powinno dzialac w miare dobrze

    }
}
