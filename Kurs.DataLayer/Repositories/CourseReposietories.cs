﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.DbContexts;
using Kurs.DataLayer.Models;

namespace Kurs.DataLayer.Repositories
{
    public class CourseReposietories
    {
        public void AddCourse(Course course)
        {

            using (var dbContext = new KursDbContext())
            {
                dbContext.CourseDbSet.Add(course);
                dbContext.SaveChanges();

            }
        }



        public void AddPresenceDay(Presence presence, string courseName, int studentId)
        {
            using (var dbContext = new KursDbContext())
            {
                presence.Student = dbContext.StudentDbSet.First(s => s.Id == studentId);
                presence.Course = dbContext.CourseDbSet.First(c => c.CourseName == courseName);
                dbContext.PresenceDbSet.Add(presence);
                dbContext.SaveChanges();
                return;
            }

        }
        public void AddHomework(Homework homework,string courseName,int studentId)
        {
            using (var dbContext = new KursDbContext())
            {
                homework.Student = dbContext.StudentDbSet.First(s => s.Id == studentId);
                homework.Course = dbContext.CourseDbSet.First(c => c.CourseName == courseName);
                dbContext.HomeworkDbSet.Add(homework);
                dbContext.SaveChanges();
                return;
            }

        }

        public bool checkCourseByName(string name)
        {
            //Student tempPesels = null;
            using (var dbContext = new KursDbContext())
            {
                //b.Title czy sie rowna tytulowi podanemu
                var tempPesels = dbContext.CourseDbSet.ToList();
                foreach (var log in tempPesels)
                {
                    if (log.CourseName == name)
                        return false;
                }
                return true;
            }

        }

        public void GetStudentsFromCourse(string name)
        {
            using (var dbContext = new KursDbContext())
            {
                //b.Title czy sie rowna tytulowi podanemu
                var tempPesels = dbContext.CourseDbSet.First(x=>x.CourseName==name);
                var tempData = tempPesels.Students.ToList();
                foreach (var log in tempData)
                {
                    Console.WriteLine(log.Id);
                    
                }
                ;
            }
        }

        public int GetMaxPresenceScore(string name)
        {
            using (var dbContext = new KursDbContext())
            {
                //b.Title czy sie rowna tytulowi podanemu
                var tempScore = dbContext.CourseDbSet.First(x => x.CourseName == name);
                return tempScore.PercentAbsence;
                                ;
            }
        }

        public int GetMaxHomeworkScore(string name)
        {
            using (var dbContext = new KursDbContext())
            {
                //b.Title czy sie rowna tytulowi podanemu
                var tempScore = dbContext.CourseDbSet.First(x=>x.CourseName==name);
                return tempScore.PercentHomework;
                ;
            }
        }
    }
}
