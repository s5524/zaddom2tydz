﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Kurs.DataLayer.DbContexts;
using Kurs.DataLayer.Models;

namespace Kurs.DataLayer.Repositories
{
    public class StudentReposietories
    {
        public bool checkStudentsByPesel(int pesel)
        {
            //Student tempPesels = null;
            using (var dbContext = new KursDbContext())
            {
                //b.Title czy sie rowna tytulowi podanemu
                var tempPesels = dbContext.StudentDbSet.ToList();
                foreach (var log in tempPesels)
                {
                    if (log.Pesel == pesel)
                        return false;
                }
                return true;
            }

        }

        public bool checkStudentsCount(int count)
        {
            var studentsCount = 0;
            using (var dbContext = new KursDbContext())
            {
                //b.Title czy sie rowna tytulowi podanemu
                var tempCount = dbContext.StudentDbSet.ToList();
                foreach (var log in tempCount)
                {
                    studentsCount++;
                }
                if (studentsCount >= count)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }

        }

        public void AddStudents(Student student)
        {

            using (var dbContext = new KursDbContext())
            {
                dbContext.StudentDbSet.Add(student);
                dbContext.SaveChanges();

            }

        }

        public void AddStudentByPeselToCourse(int pesel, string courseName)
        {
            using (var dbContext = new KursDbContext())
            {
                var list = dbContext.CourseDbSet.Include(x => x.Students);
                var courses = list.First(x => x.CourseName == courseName);
                var student = dbContext.StudentDbSet.First(x => x.Pesel == pesel);

                courses.Students.Add(student);
                dbContext.SaveChanges();

            }
        }

        public List<int> GetAllStudentsInCourse(string courseName)
        {

            List<int> studentIds = new List<int>();

            using (var dbContext = new KursDbContext())
            {
                var students = dbContext.CourseDbSet.Include(x => x.Students);
                var course = students.First(x => x.CourseName == courseName);
                foreach (var log in course.Students)
                {
                    //Console.WriteLine(log.Id);
                    studentIds.Add(log.Id);

                }

            }
            return studentIds;
        }

        public string GetName(int Id)
        {
            using (var dbContext = new KursDbContext())
            {
                var students = dbContext.StudentDbSet.First(x => x.Id == Id);
                var course = students.Name;
                return course;
            }
        }

        public int HomeworkPoints(int studentId, string courseName)
        {
            using (var dbContext = new KursDbContext())
            {
                var studentScore=0;
                var student = dbContext.HomeworkDbSet.Where(s => s.Student.Id==studentId);
                var cos = student.Where(y => y.Course.CourseName == courseName);
                foreach (var score in cos)
                {
                     studentScore =studentScore +  score.StudentScore;
                       
                }
                return studentScore;
            }
        }

        public int HomeworkMaxPoints(int studentId, string courseName)
        {
            using (var dbContext = new KursDbContext())
            {
                var maxScore = 0;
                var student = dbContext.HomeworkDbSet.Where(s => s.Student.Id == studentId);
                var cos = student.Where(y => y.Course.CourseName == courseName);
                foreach (var score in cos)
                {
                    maxScore = score.MaxScore+maxScore;

                }
                return maxScore;
            }
        }

        public int PresenceMaxPoints(int studentId, string courseName)
        {
            using (var dbContext = new KursDbContext())
            {
                var maxScore = 0;
                var student = dbContext.PresenceDbSet.Where(s => s.Student.Id == studentId);
                var cos = student.Where(y => y.Course.CourseName == courseName);
                foreach (var score in cos)
                {
                    maxScore++;

                }
                return maxScore;
            }
        }

        public int PresencePoints(int studentId, string courseName)
        {
            using (var dbContext = new KursDbContext())
            {
                var maxScore = 0;
                var student = dbContext.PresenceDbSet.Where(s => s.Student.Id == studentId);
                var cos = student.Where(y => y.Course.CourseName == courseName);
                foreach (var score in cos)
                {
                    maxScore = score.IsPresence + maxScore;

                }
                return maxScore;
            }
        }
    }
}



