﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Kurs.BusinessLayer;
using Kurs.BusinessLayer.Services;


namespace ZadDomTydz2
{
    class Program
    {
        static void Main(string[] args)
        {
            Program start = new Program();
            start.Start();
        }
        public void Start()
        {
            var exit = false;
            while (!exit)
            {
                var command = GetInt("Podaj opcję:\n1 Dodaj studentów\n2 Dodaj kurs\n3 Dodaj dzień obecności\n4 Dodaj zadanie domowe\n5 Wypisz raport\n6 Wyjscie\n");

                switch (command)
                {
                    case 1:
                        CreateStudentList();
                        break;
                    case 2:
                        CreateCourse();
                        break;
                    case 3:
                        AddPresenceDay();
                        break;
                    case 4:
                        AddHomework();
                        break;
                    case 5:
                        MakeRaport();
                        break;
                    case 6:
                        exit = true;
                        break;
                    default:
                        Console.WriteLine("Command " + command + " is not supported");
                        break;
                }
            }
        }

        public void MakeRaport()
        {
            var studentList = new StudentService();
            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
            var list = studentList.GetStudentsInCourse(courseName);
            for (int i = 0; i < list.Count; i++)
            {
                var student = studentList.GetStudentNameById(list[i]);
                Console.Write("Student "+student+" otrzymał "+studentList.PresenceScore(list[i],courseName) +"% za obecność");
                Console.WriteLine(" oraz "+ studentList.HomeworkScore(list[i], courseName) +"% za zadania domowe " +studentList.IsPased(list[i],courseName));

            }

        }

        public void AddHomework()
        {


            var addHomeworkDate = new CourseService();
            var studentList = new StudentService();

            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
            var startDate = GetDate("Podaj date pracy domowej\n");
            var maxScore = GetInt("Podaj maksymalna liczbe punktów za zadanie domowe\n");
            var list = studentList.GetStudentsInCourse(courseName);
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine("Ile punktów uzyskał " + studentList.GetStudentNameById(list[i]) + " za zadania domowe");
                var score = GetInt("\n");
                addHomeworkDate.AddHomeworks(courseName, score,maxScore, startDate, list[i]); 

            }


        }

        public void AddPresenceDay()
        {
            var addPresenceDate = new CourseService();
            var studentList = new StudentService();
            var courseName = CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n"));
            var startDate = GetDate("Podaj date obecności\n");
            var list = studentList.GetStudentsInCourse(courseName);
            for (int i = 0; i < list.Count; i++)
            {
                Console.WriteLine("czy student "+studentList.GetStudentNameById(list[i])+" był obecny");
                var presence = GetInt("0:Nie 1:Tak\n");
                addPresenceDate.AddPresenDay(list[i],presence,startDate,courseName);

            }


        }

        
        public string CheckCoursNameInDB(string text)
        {
            var checkCourseName = new CourseService();
            bool check = checkCourseName.CheckIfCourseNameIsInDB(text);
            if (check == false)
            {
                Console.WriteLine("Podana nazwa kursu jest już w bazie");
                return CheckCoursNameInDB(GetString("Podaj nazwę kursu\n")); ;
            }
            else
            {
                return text;
            }

        }

        public string CheckCoursNameInDBisExist(string text)
        {
            var checkCourseName = new CourseService();
            bool check = checkCourseName.CheckIfCourseNameIsInDB(text);
            if (check == true)
            {
                Console.WriteLine("Brak kursu w bazie w bazie");
                return CheckCoursNameInDBisExist(GetString("Podaj nazwę kursu\n")); ;
            }
            else
            {
                return text;
            }

        }

        public void CreateCourse()
        {
            CourseService addCourse = new CourseService();
            string tempCourseName = CheckCoursNameInDB(GetString("Podaj nazwę kursu\n"));
            string tempCourseOwner = GetString("Podaj imię prowadzącego\n");
            DateTime tempDateStart = GetDate("Podaj date rozoczęcia kursu\n");
            int tempPercenHomework = GetPercent("Podaj procent zadań domowych do zalicznia\n");
            int tempPrecentAbsence = GetPercent("Podaj procent obecności do zalicznia\n");
            int tempStudentsCount = GetStudentsCount(GetInt("Podaj liczbę studentów\n"));
            addCourse.AddCourse(tempCourseName, tempCourseOwner, tempDateStart, tempPercenHomework,tempPrecentAbsence);
            for (int i = 0; i < tempStudentsCount; i++)
            {
                var addStudentByPesel = new StudentService();
                var studentPesel = CheckPeselIsInDB("Podaj pesel studenta którego chcesz dodac");
                addStudentByPesel.AddStudentByPeselToCourses(studentPesel,tempCourseName);
            }
        }
        public void CreateStudentList()
        {
            
            var studentsCount = GetInt("Podaj liczbę studentówj jaką chcesz wprowadzic\n");
            for (int i = 0; i < studentsCount; i++)
            {
                StudentService addStudent = new StudentService();
                string tempName = GetString("Podaj imię\n");
                string tempSurname = GetString("Podaj nazwisko\n");
                DateTime tempDate = GetDate("Podaj datę urodzenia\n");
                int tempPesel = GetPesel("Podaj Pesel");
                int tempSex = GetSex("Podaj płeć\n");
                addStudent.addStudent(tempName, tempSurname, tempDate, tempPesel, tempSex);
            }
            

        }

        public int GetStudentsCount(int studentCount)
        {
            int studenCount;

            var studentCheckCount = new StudentService();
            bool check = studentCheckCount.CheckHowMuchStudentsIs(studentCount);
            if (check == false)
            {
                Console.Write("Studentów jest mniej niż miejsc na kursie\n");
                return GetStudentsCount(GetInt("Podaj liczbę studentów\n"));
            }
            else
            {
                return studentCount;
            }
        }


        public static DateTime GetDate(string text)
        {
            DateTime tempmsg;
            Console.Write(text);

            while (!DateTime.TryParse(Console.ReadLine(), out tempmsg))
            {
                Console.WriteLine("Not an DateTime - try again...");
            }

            return tempmsg;
        }
        public static int GetInt(string message)
        {
            int number;
            Console.Write(message);

            while (!int.TryParse(Console.ReadLine(), out number))
            {
                Console.WriteLine("Not an integer number - try again...");
            }

            return number;
        }
        public int GetSex(string text)
        {
            int tempmsg;
            Console.Write(text);

            while (!int.TryParse(Console.ReadLine(), out tempmsg))
            {
                Console.WriteLine("Not an integer number - try again...");
            }
            if (tempmsg > 0 && tempmsg < 3)
            {
                return tempmsg;
            }
            else
            {
                
                return GetSex("Złe dane podaj ponownie 1:M 2:K\n");
            }
        }

        public static string GetString(string text)
        {
            string tempString;
            Console.Write(text);

            tempString = Console.ReadLine();

            return tempString;

        }

        public static int GetPesel(string text)
        {
            int tempPesel;
            Console.WriteLine(text);
           
            while (!int.TryParse(Console.ReadLine(), out tempPesel))
            {
                Console.WriteLine("Not an percente number - try again...");
            }

            var studentCheckPesel = new StudentService();
            bool check = studentCheckPesel.CheckIfStudentExistsInDBByPesel(tempPesel);
            if (check == false)
            {
                Console.WriteLine("Pesel jest juz w bazie");
                return GetPesel("Podaj pesel");
            }
            else
            {
                return tempPesel;
            }
            
        }
        public int GetPercent(string text)
        {
            int tempmsg;
            Console.Write(text);

            while (!int.TryParse(Console.ReadLine(), out tempmsg))
            {
                Console.WriteLine("Not an integer number - try again...");
            }
            if (tempmsg >= 0 && tempmsg <= 100)
            {
                return tempmsg;
            }
            else
            {

                return GetPercent("Złe dane \n");
            }
        }

        public static int CheckPeselIsInDB(string text)
        {
            int tempPesel;
            Console.WriteLine(text);

            while (!int.TryParse(Console.ReadLine(), out tempPesel))
            {
                Console.WriteLine("Not an percente number - try again...");
            }

            var studentCheckPesel = new StudentService();
            bool check = studentCheckPesel.CheckIfStudentExistsInDBByPesel(tempPesel);
            if (check == true)
            {
                Console.WriteLine("Brak peselu w bazie");
                return CheckPeselIsInDB("Podaj pesel");
            }
            else
            {
                return tempPesel;
            }

        }


    }
}
